import Hello from '@/components/Hello'
import Preview from '@/components/Preview'
import LaravelKit from '@/components/LaravelKit'
import Features from '@/components/Features'
import Elements from '@/components/Elements'

export const routes = [
  {
    path: '/',
    name: 'Hello',
    component: Hello
  },
  {
    path: '/preview',
    name: 'Preview',
    component: Preview
  },
  {
    path: '/laravel-starter-kit',
    name: 'LaravelKit',
    component: LaravelKit
  },
  {
    path: '/features',
    name: 'Features',
    component: Features
  },
  {
    path: '/elements',
    name: 'Elements',
    component: Elements
  }
]
